<?php

    class BirdRun {

        private $bird;

        public function __construct(Bird $bird) {
            $this->bird = $bird;
        }

        public function run() {
            $flyspeed = $this->bird->fly();
            //...
        }

    }