<?php

    class Bird {

        public function fly() {
            $flyspeed = 10;
            return $flyspeed;
        }

    }

    class Duck extends Bird {

        public function fly() {
            $flyspeed = 8;
            return $flyspeed;
        }

        public function swim() {
            $swimSpeed = 2;
            return $swimSpeed;
        }

    }

    class Penguin extends Bird {

        public function fly() {
            return 'I can not fly!';
        }

        public function swim() {
            $swimSpeed = 4;
            return $swimSpeed;
        }

    }