<?php

/*
 * SOLID
 * O
 * Open/Closed principle
 * Принцип Открытости/Закрытости
 * */

    $logger = new DBLogger();
    $product = new Product($logger);
    $product->setPrice(10);