<?php

/*
 * SOLID
 * S
 * Single responsibility principle
 * Принцип единственной ответственности
 * */

    $logger = new Logger();
    $product = new Product($logger);
    $product->setPrice(10);